class Api::Internal::<%=cp%>Controller < ApplicationController
  before_filter :intercept_html_requests
  before_filter :authenticate_user!
  load_and_authorize_resource
  layout false
  respond_to :json
  before_action :set_<%=u%>, only: [:show, :update, :destroy]

  def default_serializer_options
    {
      root: false
    }
  end

  def index
    @<%=up%> = <%=name%>.where(PARENT: params[:<%=u%>_id])
    if @<%=up%>
      render json: @<%=up%>
    else
      render json: "Cannot find <%=h%> record", status: :unprocessable_entity
    end
  end

  def show
    if @<%=u%>
      render json: @<%=u%>
    else
      render json: "Cannot find <%=h%> record", status: :unprocessable_entity
    end
  end

  def update
    if @<%=u%>.update(<%=u%>_params)
      render json: {}, status: 200
    else
      render json: @<%=u%>.errors.full_messages, status: :unprocessable_entity
    end
  end

  def create
    @<%=u%> = <%=name%>.create(<%=u%>_params)
    if @<%=u%>.persisted?
      render json: @<%=u%>, status: 201
    else
      render json: @<%=u%>.errors.full_messages, status: :unprocessable_entity
    end
  end
  
  def destroy
    if @<%=u%>.destroy
      render json: {}, status: 204
    else
      render json: @<%=u%>.errors.full_messages, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_<%=u%>
      @<%=u%> = <%=name%>.find(params[:id]) 
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def <%=u%>_params
      params.require(:<%=u%>).permit(<%=params%>)
    end
end
