= Ngapi

Usage:

rails g ngapi SportHistory client_id:integer:index start_date:datetime notes:text awards:text

OUTPUT:
    generate  model SportHistory client_id:integer:index start_date:datetime notes:text awards:text --no-helper --no-assets --no-model-specs --no-controllers-specs --no-views-specs --no-test-framework
      invoke  active_record
      create    db/migrate/20140223233822_create_sport_histories.rb
      create    app/models/sport_history.rb
    generate  serializer SportHistory
      create  app/serializers/sport_history_serializer.rb
      create  app/controllers/api/v1/sport_histories_controller.rb
      create  app/assets/sport_histories.html

CREATES:
Creates migration
Creates model
Adds to route /api/v1/
Runs migration
Creates controller
Creates a serializer
Creates Angular form boilerplate


Generator to create rails boilerplate for integration with AngularJS / Restangular

MIT-LICENSE