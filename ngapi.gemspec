$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "ngapi/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "ngapi"
  s.version     = Ngapi::VERSION
  s.authors     = ["Adam Perkins"]
  s.email       = ["a@aap.bz"]
  s.homepage    = "http://aap.bz"
  s.summary     = "Generator to create rails boilerplate for integration with AngularJS / Restangular"
  s.description = "Generator to create rails boilerplate for integration with AngularJS / Restangular"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails"

end
