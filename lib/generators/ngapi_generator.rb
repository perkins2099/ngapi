class NgapiGenerator < Rails::Generators::NamedBase
  desc "NG API GEN"
  source_root File.expand_path('../templates', __FILE__)
  argument :name, type: :string
  argument :fields, type: :array, default: []
  class_option :form, :type => :boolean, :default => true, :desc => "Include basic form for migration"
  

  def create_model
    g = "model #{name} #{fields.join(" ")} --no-helper --no-assets --no-model-specs --no-controllers-specs --no-views-specs --no-test-framework"  
    generate g
  end

  def create_serializer
    generate "serializer #{name} #{fields.join(" ")}"
  end

  def create_controller
    c_name = name.underscore.pluralize + "_controller"
    template "controller.rb", "app/controllers/api/internal/#{c_name}.rb"
  end
  def create_ctrl
    c_name = name.underscore.pluralize + "Ctrl"
    template "ctrl.js.coffee", "app/assets/#{c_name}.js.coffee" if options.form?
  end

  #route add
  def router
    sentinel = 'namespace :internal do'
    route = "      resources :#{up}, except: [:new, :edit]"
    in_root do
      gsub_file 'config/routes.rb', /(#{Regexp.escape(sentinel)})/mi do |match|
        "#{match}\n#{route}\n"
      end
    end
    
  end

  #Make sure no one can get HTML
  def modify_app_cont
    sentinel = 'class ApplicationController < ActionController::Base'
    method = "  def intercept_html_requests\n    redirect_to('/') if request.format == Mime::HTML\n  end"

    do_not_insert = false
    File.open("app/controllers/application_controller.rb").each_line do |line|
      if line.include?("def intercept_html_requests")
        do_not_insert = true
      end
    end
    if do_not_insert == false
      in_root do
        gsub_file 'app/controllers/application_controller.rb', /(#{Regexp.escape(sentinel)})/mi do |match|
          "#{match}\n\n#{method}\n"
        end
      end
    end
  end #End Method

  def form_creation
    c_name = name.underscore.pluralize
    template "form.html", "app/assets/#{c_name}.html" if options.form?
  end

  private

    # def field_array
    #   fields.split(" ")
    # end
  
    def c(cap = true)
      if cap
        name.camelize
      else
        name.camelize(:lower)
      end
    end
    def cp(cap = true)
      if cap
        name.camelize.pluralize
      else
        name.camelize(:lower).pluralize
      end
    end
    def u
      name.underscore
    end
    def up
      name.underscore.pluralize
    end

    def h
      name.underscore.humanize.downcase
    end
    def ht
      name.underscore.humanize.titleize
    end

    def params
      x = []
      fields.each do |field|
        x << field.split(":")[0]
      end

      ":" + x.join(", :")
    end

end
