APP_NAME.controller '<%=c%>Ctrl', ($scope, currentUser, $state, authService, Restangular) ->

  $scope.<%=cp(false)%> = []
  $scope.new<%=c%>  = {}

  $scope.notice =
    message: null
    alert: "notice"

  $scope.fetch<%=cp%>  = ->
    MODEL.getList("<%=up%>").then ((response) ->
      $scope.<%=cp(false)%>  = response
    ), (response) ->
      $scope.notice.message = response.data

  $scope.add<%=c%>  = ->
    MODEL.post("<%=up%> ", $scope.new<%=c%> ).then ((response)->
      $scope.<%=cp(false)%>.push(response)
    ), (response) ->
      $scope.notice.message = response.data

  $scope.remove<%=c%>  = (item) ->
    index = $scope.<%=cp(false)%> .indexOf(item)
    $scope.<%=cp(false)%>.splice(index,1)
    item.remove().then ((response) ->
    ), (response) ->
      $scope.notice.message = response.data

  $scope.update<%=c%>  = (item) ->
    item.put().then ((response)->
      index = $scope.<%=cp(false)%>.indexOf(item)
    ), (response) ->
      $scope.notice.message = response.data


  $scope.fetch<%=cp%>()
